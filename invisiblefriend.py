#!/usr/bin/python
# coding=utf8

import random,string
from email.mime.text import MIMEText
import smtplib
import getpass

s = smtplib.SMTP_SSL()
s.connect('smtp.gmail.com', 465)
pwd = getpass.getpass(prompt='E-mail password: ')
s.login('2piruben@gmail.com',pwd)

gifters=[]
gifters.append(['David','kromenaguer@gmail.com','Rubén Ferrer'])
gifters.append(['Rubén Ferrer','rubenferrerm@gmail.com','David'])
gifters.append(['Noemí','noeaguilo@gmail.com','Dani'])
gifters.append(['Dani','danielgilbayo@gmail.com','Noemí'])
gifters.append(['Jose','joselmgomez@gmail.com','Rubén Pérez'])
gifters.append(['Rubén Pérez','2piruben@gmail.com','Jose'])

gifteds=list(gifters)
giftedbad=True
while (giftedbad):
    random.shuffle(gifteds)
    giftedbad=False
    for gifter,gifted in zip(gifters,gifteds):
        if gifter[0]==gifted[0] or gifter[0]==gifted[2]:
            giftedbad=True
#        print gifter[0],gifted[0] 
                

for gifter,gifted in zip(gifters,gifteds):
#	print gifter[0],gifted[0]
    name={'amigo':gifted[0]}
    msgtmp= string.Template('''Hola!
Si estás leyendo esto es porque eres una persona maravillosa dispuesta a 
regalar algo a alguien de forma anónima. Eres UN AMIGO INVISIBLE!
No obstante, como es complicado elegir a quien regalar, te lo voy a poner fácil.
Me ha dicho un pajarito que ${amigo} todavía no tiene quien le regale.
Así que, ¿A qué esperas?



Firmado,
Un amigo virtual''')

    msg=msgtmp.substitute(name)
    msg = MIMEText(msg)
    msg['Subject']= 'Eres un amigo invisible!'
    msg['From']='Un Amigo'
    msg['To ']=gifter[1]
    s.sendmail('Amigo Invisible',gifter[1],msg.as_string())

s.quit()

