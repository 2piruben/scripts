"""""""""""""""""""""""""""""""""""""""""""""""""
"   VI OPTION FILE
"
""""""""""""""""""""" R.P-C 24 Apr 13

" Highlightin of code
syntax on

" Highlightin color code
colorscheme desert
highlight Normal ctermbg=DarkGrey

" Settings og  the linenumber 
highlight LineNr guifg=DarkGrey guibg=Black ctermfg=DarkGrey ctermbg=Black
set number

" Line Wrapping
set lbr

" Put all te backup and swap files *.swp is a separate file
" to avoid the creation of multiple files in the same folder
" The folders specified should exist!!! 
set backupdir=~/.vim/backup//
set directory=~/.vim/swap//

" Tabulation size
set tabstop=4
