
# MacPorts Installer addition on 2012-05-08_at_10:42:25: adding an appropriate PATH variable for use with MacPorts.
export PATH=/opt/local/bin:/opt/local/sbin:$PATH
# Finished adapting your PATH environment variable for use with MacPorts.

# Adding my own scripts to the PATH

export PATH=~/Documents/scripts:$PATH 

# Prompt line lookout
PS1="\u@\h:\w$ "

# Setting PATH for EPD_free-7.3-2
# The orginal version is saved in .profile.pysave
source ~/auto/07p/cmds/auto.env.sh
PYTHONPATH=$PYTHONPATH:$HOME/:$HOME/Ciencia/:$HOME/Ciencia/PyDSTool/:$HOME/Ciencia/PyDSTool/tests/
export PYTHONPATH
PATH="/Library/Frameworks/Python.framework/Versions/Current/bin:/usr/local/bin:${PATH}"
export PATH

# Added by Canopy installer on 2013-06-17
# VIRTUAL_ENV_DISABLE_PROMPT can be '' to make bashprompt show that Canopy is active, otherwise 1
VIRTUAL_ENV_DISABLE_PROMPT=1 source /Users/ruben/Library/Enthought/Canopy_32bit/User/bin/activate
