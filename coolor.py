##### COOLOR MODULE #######################           Ruben Perez-Carrasco 7 July 2015

# color dictionary contains colors. Names are extracted from x11names colors of xcolor LaTeX package because 
# name colours where very intuitive

# Names are defined in the dictionary colorint{} as RGB lists with integers (0-255).
# Digits after the color indicate darker versions that follow the relationship Color2=Color1*14/15
# They could be created like that, but I decided to hardcode them since are the base colornames of x11names.
# Keep in mind that Sometimes the names are intuitive for one of the 4 instances of every colorname but not for the rest, 
# for instance Brown4 or Chocolate4 are intuitive, but not Brown1 or Chocolate1.

colorint={"AntiqueWhite1":[255,239,219],"AntiqueWhite2":[238,223,204],"AntiqueWhite3":[205,192,176],"AntiqueWhite4":[139,130,120],
"Aquamarine1":[121,243,202],"Aquamarine2":[112,226,188],"Aquamarine3":[96,192,159],"Aquamarine4":[64,129,107],
"Azure1":[227,241,241],"Azure2":[211,223,223],"Azure3":[158,167,167],"Azure4":[122,130,130],
"Bisque1":[255,228,196],"Bisque2":[238,213,183],"Bisque3":[205,183,158],"Bisque4":[139,125,107],
"Blue1":[0,0,255],"Blue2":[0,0,238],"Blue3":[0,0,205],"Blue4":[0,0,139],
"Brown1":[255,64,64],"Brown2":[238,59,59],"Brown3":[205,51,51],"Brown4":[139,35,35],
"Burlywood1":[255,211,155],"Burlywood2":[238,197,145],"Burlywood3":[205,170,125],"Burlywood4":[139,115,85],
"CadetBlue1":[152,245,255],"CadetBlue2":[142,229,238],"CadetBlue3":[122,197,205],"CadetBlue4":[83,134,139],
"Chartreuse1":[127,255,0],"Chartreuse2":[118,238,0],"Chartreuse3":[102,205,0],"Chartreuse4":[69,139,0],
"Chocolate1":[255,127,36],"Chocolate2":[238,118,33],"Chocolate3":[205,102,29],"Chocolate4":[139,69,19],
"Coral1":[255,114,86],"Coral2":[238,106,80],"Coral3":[205,91,69],"Coral4":[139,62,47],
"Cornsilk1":[255,248,220],"Cornsilk2":[238,232,205],"Cornsilk3":[205,200,177],"Cornsilk4":[139,135,120],
"Cyan1":[0,255,255],"Cyan2":[0,238,238],"Cyan3":[0,205,205],"Cyan4":[0,139,139],
"DarkGoldenrod1":[255,185,15],"DarkGoldenrod2":[238,173,14],"DarkGoldenrod3":[205,149,12],"DarkGoldenrod4":[139,101,8],
"DarkOliveGreen1":[],"DarkOliveGreen2":[],"DarkOliveGreen3":[],"DarkOliveGreen4":[],
"DarkOrange1":[],"DarkOrange2":[],"DarkOrange3":[],"DarkOrange4":[],
"DarkOrchid1":[],"DarkOrchid2":[],"DarkOrchid3":[],"DarkOrchid4":[],
"DarkSeaGreen1":[],"DarkSeaGreen2":[],"DarkSeaGreen3":[],"DarkSeaGreen4":[],
"DarkSlateGray1":[],"DarkSlateGray2":[],"DarkSlateGray3":[],"DarkSlateGray4":[],
"DeepPink1":[],"DeepPink2":[],"DeepPink3":[],"DeepPink4":[],
"DeepSkyBlue1":[],"DeepSkyBlue2":[],"DeepSkyBlue3":[],"DeepSkyBlue4":[],
"DodgerBlue1":[],"DodgerBlue2":[],"DodgerBlue3":[],"DodgerBlue4":[],
"Firebrick1":[],"Firebrick2":[],"Firebrick3":[],"Firebrick4":[],
"Gold1":[],"Gold2":[],"Gold3":[],"Gold4":[],
"Goldenrod1":[],"Goldenrod2":[],"Goldenrod3":[],"Goldenrod4":[],
"Green1":[],"Green2":[],"Green3":[],"Green4":[],
"Honeydew1":[],"Honeydew2":[],"Honeydew3":[],"Honeydew4":[],
"HotPink1":[],"HotPink2":[],"HotPink3":[],"HotPink4":[],
"IndianRed1":[],"IndianRed2":[],"IndianRed3":[],"IndianRed4":[],
"Ivory1":[],"Ivory2":[],"Ivory3":[],"Ivory4":[],
"Khaki1":[],"Khaki2":[],"Khaki3":[],"Khaki4":[],
"LavenderBlush1":[],"LavenderBlush2":[],"LavenderBlush3":[],"LavenderBlush4":[],
"LemonChiffon1":[],"LemonChiffon2":[],"LemonChiffon3":[],"LemonChiffon4":[],
"LightBlue1":[],"LightBlue2":[],"LightBlue3":[],"LightBlue4":[],
"LightCyan1":[],"LightCyan2":[],"LightCyan3":[],"LightCyan4":[],
"LightGoldenrod1":[],"LightGoldenrod2":[],"LightGoldenrod3":[],"LightGoldenrod4":[],
"LightPink1":[],"LightPink2":[],"LightPink3":[],"LightPink4":[],
"LightSalmon1":[],"LightSalmon2":[],"LightSalmon3":[],"LightSalmon4":[]}