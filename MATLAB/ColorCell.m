%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% ColorPalette
%					Created:   Ruben Perez-Carrasco 31 Aug '12
%
%------
% Function to create a cell containing a set of arguments for the 
% coloring plot function
%
% Usage: Call the function to create a cell array, and use for each
%        call to plot a different row of the cell as D{indx,:}
%        being indx the index of the cell wanted
%------
%

function D = ColorCell()

N=5; % Number of color defined

colorE=[1 0.412 0;... % My nice palette!!
        0 0.2 0.6;...
        0 0.392 0;...
        0.4078 0.1333 0.5451;...
        0.5, 0, 0];
colorF=[1 0.6 0;...
        0 0.2 1;...
        0 0.6 0;...
        0.6980 0.2275 0.9333;...
        1 0.2 0.2];

for ind=1:N
    D{ind,1}='MarkerFaceColor';
    D{ind,2}=colorF(ind,:);
    D{ind,3}='MarkerEdgeColor';
    D{ind,4}=colorE(ind,:);
end
end
    