%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% CREATEFIGURE
%					Created:   Ruben Perez-Carrasco 7 Aug '12
%                   Modified:  31 Aug '12
%
%% Function to automatize the used of ml2pdf working with matlabfrag and Mac

% Set of the Path environments to use pdftex 
function createfigure();

setenv('PATH', [getenv('PATH') ':/usr/texbin']);
setenv('PATH', [getenv('PATH') ':/usr/local/bin']);


% -- PARAMETERS ------------------------------------------

XLength=  14;% Horizontal Length of the axis (cm*xfactor)
YLength = XLength/1.62;% Vertical length of axis (golden ratio)
DeltaX = 2.2;% Space for left(Y) labels
DeltaY = 2; % Space for bottom(X) labels
xlaboff= 0.7; % fraction of DeltaX for left(Y) label
ylaboff= 0.7; % fraction of DeltaY for bottom(X) label
LabelFont = 22; % Size of the font for the axis labels
%TickFont = 16; % Size of the font for the ticks (numbers) labels
TickFont = 20; 
%---------------------------------------------------------



set(gcf,'DefaultAxesColorOrder');


% Axes Font settings
set (gca,'Fontsize',TickFont); % Ticks label fonts
set (get(gca,'XLabel'),'Fontsize',LabelFont); % label title font
set (get(gca,'YLabel'),'Fontsize',LabelFont); % ''

% Axes and figure sizes and positions
AxisBox = [DeltaX DeltaY XLength YLength];
FigBox = [0 0 XLength+1.5*DeltaX YLength+1.5*DeltaY];
xlabelPos = [-xlaboff*DeltaX 0.5*YLength 0]; % default origin at axis 0
ylabelPos = [0.5*XLength -ylaboff*DeltaY 0];

% Setting of figure and axis
set(gcf,'Units','centimeters'); 
set(gca,'Units','centimeters'); % Useful to avoid resize
set(gcf,'Position',FigBox);
set(gca,'position',AxisBox);

% Setting of label positions
ylabh = get(gca,'XLabel');
set(ylabh,'Units','centimeters');
xlabh = get(gca,'YLabel');
set(xlabh,'Units','centimeters');
set(ylabh,'HorizontalAlignment','center','VerticalAlignment','middle',...
    'Position',ylabelPos);
set(xlabh,'HorizontalAlignment','center','VerticalAlignment','middle',...
    'Position',xlabelPos);

% Call to mlf2pdf (LaTeX treatment)
mlf2pdf;

% The change of units to centimeters may create strange behaviour
% on the figure working with it after using createfigure. This can 
% be avoided by closing and openin the figure every time mlf2pdf is
% called or by adding a line to createfigure returning to default
% units.

end

